<?php

namespace App\Permissions;

use App\File\Directory;
use App\File\File;

class PermissionFixer
{
  public $www_should_own = [
    'public/media',
    'public/magento/media',
    'public/wp-content/uploads',
    'public/wp/wp-content/uploads',
    'public/cache/',
    'public/storage/',
    'public/var/',
  ];
  public function handle( $dir = '/var/www' ) {
    // List all subdirectories in /var/www
  }
}
