<?php

namespace App;

use App\File\ApacheConf;
use App\File\NginxConf;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    public $websites = [];

    public function __construct()
    {
        $this->ningx_status = ``;
        $this->uptime = trim(`uptime 2>/dev/null`);
        $this->distro = '';
        if (preg_match('/Operating System:\s*([^\n]*)/', `hostnamectl`, $matches)) {
            $this->distro = $matches[1];
        } else if (preg_match('/System Version:\s*([^\n]*)/', `system_profiler SPSoftwareDataType`, $matches)) {
            $this->distro = $matches[1];
        }

        $disk_space = `df | grep -E "\/sda|\/root|\/disk1s1"`;
        $this->disk_space_available = '';
        // Filesystem, 512-blocks, Used, Available, Capacity, iused, ifree, %iused, M
        $pattern = '/^([\S]+)\s+([\S]+)\s+([\S]+)\s+([\S]+)\s+([\S]+)\s+([\S]+)\s+/';
        if (preg_match($pattern, $disk_space, $matches)) {
            $this->disk_space_available = $matches[4];
            $this->disk_capacity = $matches[5];
        }
        $this->versions = [
            'nginx'     => $_SERVER['SERVER_SOFTWARE'] ?? '',
            'wwwtools'  => trim(`git rev-parse HEAD`),
            'deplutils' => trim(`cd /root/deplutils && git rev-parse HEAD`),
            'git'       => trim(`git --version`),
            'certbot'   => trim(`certbot --version`),
        ];
        $this->pear_config_installed = false !== strpos(get_include_path(), 'pear');
        // $assertions = new Assertions();
        $this->assertions = $this->getAssertions();
        $this->websites = $this->getWebsites();
        foreach ($this->websites as &$website) {
            if (! isset($website['dir'])) {
                continue;
            }
            $dir = $website['dir'];
            if (! is_dir($dir)) {
                continue;
            }
            try {
                chdir($dir);
                $website['git_branch'] = trim(`git rev-parse --abbrev-ref HEAD`);
                $website['git_remote'] = trim(`git config --get remote.origin.url`);
            } catch (\Exception $e) {

            }
        }
    }
    public function getAssertions()
    {
        $command = env('ASSERTIONS_COMMAND');
        if (! $command) {
            return ['No assertion command specified'];
        }
        $result = shell_exec($command);
        $lines = explode("\n\n", $result);
        $lines = array_map('trim', $lines);
        foreach ($lines as &$line) {
            $line = preg_replace('/\s+/', ' ', $line);
        }
        return array_filter($lines);
    }
    public function getWebsites()
    {
        if (env('HOST_WEBSERVER', 'apache') === 'apache' && ! $this->pear_config_installed) {
            return [];
        }
        $conf_dir = env('HOST_CONF_DIR', 'DEFAULT');
        if ('DEFAULT' === $conf_dir) {
            if (env('HOST_WEBSERVER', 'apache') === 'nginx') {
                $conf_dir = '/etc/nginx/sites-available';
            } else {
                $conf_dir = '/etc/apache2/sites-available';
            }
        }
        $files = glob("$conf_dir/*");
        $websites = [];
        foreach ($files as $file) {
            if (preg_match('/default$/', $file)) {
                // skip nginx default.
                continue;
            }
            if (env('HOST_WEBSERVER', 'apache') === 'nginx') {
                $config = new NginxConf($file);
            } else {
                $config = new ApacheConf($file);
            }
            $websites = array_merge($websites, $config->getWebsites());
        }
        return $websites;
    }
}
