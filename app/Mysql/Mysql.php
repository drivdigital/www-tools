<?php

namespace App\Mysql;

use Illuminate\Support\Facades\DB;

class Mysql {
  public function exec( $args ) {
    if ( is_string( $args ) ) {
      $args = [ '%', $args ];
    }
    $sql = call_user_func_array( 'sprintf', $args );
    if ( ! $sql ) {
      return false;
    }
    if ( strpos( strtolower( $sql ), 'select' ) === 0 ) {
      return DB::select( DB::raw( $sql ) );
    }
    if ( strpos( strtolower( $sql ), 'show' ) === 0 ) {
      return DB::select(
        DB::raw( $sql ) );
    }
    return DB::statement( DB::raw( $sql ) );
    // echo "Would do:\n $sql\n";
  }
}
