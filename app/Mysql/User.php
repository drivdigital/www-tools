<?php

namespace App\Mysql;


class User extends Mysql {
  public $username;
  public $host;
  public $password;

  function __construct( $username, $host = 'localhost' ) {
    $this->username = $username;
    $this->host = $host;
  }
  public function exists() {
    $format = "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '%s' AND host = '%s') as existing";
    $result = $this->exec( [ $format, $this->username, $this->host ] );
    if ( ! count( $result ) ) {
      return false;
    }
    return $result[0]->existing;
  }

  /**
   * Save
   * @param boolean|string $password False will generate a random password.
   */
  public function save( $password = false ) {
    if ( ! $password ) {
      $password = str_random( 20 );
    }
    $this->password = $password;
    $format = "CREATE USER '%s'@'%s' IDENTIFIED BY '%s'";
    if ( $this->exists() ) {
      return false;
    }
    $result = $this->exec( [ $format, $this->username, $this->host, $password ] );
    // $format = "SET PASSWORD FOR '%s'@'%s' = PASSWORD('%s');";
    // $this->exec( [ $format, $this->username, $this->host, $password ] );
    return $result;
  }
  public function delete() {
    $format = "DROP USER '%s'@'%s'";
    if ( ! $this->exists() ) {
      return false;
    }
    return $this->exec( [ $format, $this->username, $this->host ] );
  }
  static function list() {
    $format = "SELECT User, Host FROM mysql.user";
    $mysql = new Mysql();
    return $mysql->exec( [ $format ] );
  }

  /**
   * Grant
   * @param  string $database
   * @param  string $grants
   * @return mixed
   */
  public function grant( $database = false, $grants = 'CREATE, REFERENCES, LOCK TABLES, EVENT, SHOW VIEW, ALTER, SELECT, INSERT, UPDATE, DELETE, TRIGGER, CREATE TEMPORARY TABLES', $with_grant = false ) {
    if ( ! $database ) {
      $database = $this->username;
    }
    $format = "GRANT %s ON %s.* TO '%s'@'%s'";
    if( $with_grant ) {
      //GRANT ALL PRIVILEGES ON mydb.* TO 'myuser'@'%'
      $format .= ' WITH GRANT OPTION;';
    }

    return $this->exec( [ $format, $grants, $database, $this->username, $this->host ] );
  }
}
