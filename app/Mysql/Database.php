<?php

namespace App\Mysql;

class Database extends Mysql {

  protected $name;
  protected $branch;

  function __construct( $name, $branch ) {
    $this->name = $name;
    $this->branch = $branch;
  }

  /**
   * Get combined name
   * @return string NAME_BRANCH
   */
  public function get_combined_name() {
    return "{$this->name}_{$this->branch}";
  }

  /**
   * Exists
   * @return boolean
   */
  public function exists() {
    $format = "SHOW DATABASES like '%s'";
    $result = $this->exec( [ $format, $this->get_combined_name() ] );
    return ( count( $result ) );
  }

  /**
   * Save
   * @return mixed
   */
  public function save() {
    if ( $this->exists() ) {
      return false;
    }
    $format = 'CREATE DATABASE %s';
    return $this->exec( [ $format, $this->get_combined_name() ] );
  }

  /**
   * Save
   * @return mixed
   */
  public function import( $file ) {
    if ( ! $this->exists() ) {
      return false;
    }

    $info = pathinfo( $file );
    $unzip = '';
    if ( 'gz' == @$info['extension'] ) {
      $unzip = '| gunzip';
    }
    if ( env( 'APP_ENV' ) == 'local' ) {
      echo sprintf( 'cat %s %s| mysql -u %s -p%s %s',
        $file,
        $unzip,
        env( 'DB_USERNAME', 'root' ),
        env( 'DB_PASSWORD', '' ),
        $this->get_combined_name()
      ) ."\n";
    }
    exec( sprintf( 'cat %s %s| mysql -u %s -p%s %s',
      $file,
      $unzip,
      env( 'DB_USERNAME', 'root' ),
      env( 'DB_PASSWORD', '' ),
      $this->get_combined_name()
    ), $output, $return );
    var_dump( $output, $return );
  }

  /**
   * Delete
   * @return mixed
   */
  public function delete() {
    if ( ! $this->exists() ) {
      return false;
    }
    $format = 'DROP DATABASE %s';
    return $this->exec( [ $format, $this->get_combined_name() ] );
  }

  /**
   * Get tables
   * @return mixed
   */
  public function get_tables() {
    if ( ! $this->exists() ) {
      return false;
    }
    $format = 'SHOW TABLES IN %s';
    return $this->exec( [ $format, $this->get_combined_name() ] );
  }

  /**
   * Get tables
   * @return mixed
   */
  public function drop_table( $table ) {
    if ( ! $this->exists() ) {
      return false;
    }
    if ( ! $table ) {
      return false;
    }
    if ( ! strpos( $table, '.' ) ) {
      $table = $this->get_combined_name() .'.'. $table;
    }
    $format = 'DROP TABLE %s';
    return $this->exec( [ $format, $table ] );
  }

  /**
   * Get user
   * @return boolean|object User on success
   */
  public function get_user( $password = false ) {
    $user = new User( $this->get_combined_name() );
    return $user;
  }

  /**
   * Create user
   * @param  boolean|string $password
   * @return boolean|object User on success
   */
  public function create_user( $password = false ) {
    $user = new User( $this->get_combined_name() );
    if ( ! $user->save( $password ) ) {
      return $user;
    }
    $user->grant( $this->get_combined_name() );
    return $user;
  }

  /**
   * List
   * @param  boolean $include_size (Optional, default = true)
   * @return array
   */
  static function list( $include_size = true ) {
    $sql = <<<SQL
SELECT
table_schema "name" %s
FROM information_schema.tables
GROUP BY table_schema;
SQL;
    $size_query = '';
    if ( $include_size ) {
      $size_query = ', Round(Sum(data_length + index_length) / 1024 / 1024, 1) "size"';
    }
    $mysql = new Mysql;
    return $mysql->exec( [ $sql, $size_query ] );
  }

  /**
   * Disable foreign keys
   */
  public function disable_foreign_keys() {
    $this->exec( [ 'SET FOREIGN_KEY_CHECKS=0;' ] );
  }

  /**
   * Enable foreign keys
   */
  public function enable_foreign_keys() {
    $this->exec( [ 'SET FOREIGN_KEY_CHECKS=1;' ] );
  }
}
