<?php

namespace App\File;

/**
 *
 */
class File
{
  protected $path;
  public $permissions = '664';
  public $group = null;

  function __construct( $path ) {
    $this->path = $path;
  }

  public function get_path() {
    return $this->path;
  }
}