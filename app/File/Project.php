<?php

namespace App\File;

/**
 *
 */
class Project extends Directory
{
  /**
   * Get project dir
   * Warning! recursive function
   * @param  string         $cwd (Optional) Web project root directory
   * @return Project        Working dir on success. False on failure
   */
  static function get_from_path( $cwd = '' ) {
    if ( ! $cwd ) {
      $cwd = getcwd();
    }
    $cwd = realpath( $cwd );
    $parts = explode( '/', $cwd );
    $parts = array_filter( $parts );
    if ( count( $parts ) < 2 ) {
      return null;
    }
    $files = scandir( $cwd );
    if ( in_array( 'public', $files ) ) {
      return new Project( $cwd );
    }
    if ( in_array( 'index.php', $files ) && ! in_array('public', $parts ) ) {
      return new Project( $cwd );
    }
    return self::get_from_path( dirname( $cwd ) );
  }
}