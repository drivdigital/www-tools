<?php

namespace App\File;

/**
 * Apache config
 */
class ApacheConf
{
    /**
     * Construct
     *
     * @param string $file File.
     */
    function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * To Array
     *
     * @return array
     */
    public function getWebsites()
    {
        include_once 'Config.php';
        $config = new \Config();
        $root = $config->parseConfig($this->file, 'apache');
        return $this->parseApacheConf($root, $this->file);
    }

    /**
     * Parse Config
     *
     * @param Config $root (From pear package)
     *
     * @return array Websites
     */
    public function parseApacheConf($root)
    {
        $websites = [];
        foreach ($root->children as $vhost) {
            if ($vhost->getName() == '') {
                continue;
            } else if ($vhost->getName() == 'IfModule') {
                $websites = array_merge($websites, $this->parseApacheConf($vhost, $this->file));
                continue;
            } else if ($vhost->getName() !== 'VirtualHost') {
                $websites[] = [
                    'error' => [
                        'file'    => $this->file,
                        'message' => 'Unknown element, ' . $vhost->getName(),
                    ],
                ];
                continue;
            }
            $aliases = [];
            $count = $vhost->countChildren('directive', 'ServerAlias');
            for ($i = 0; $i < $count; $i++) {
                $item = $vhost->getItem('directive', 'ServerAlias', null, null, $i);
                $aliases[] = $item->getContent();
            }
            $websites[] = [
                'listen'     => $vhost->getAttribute(0),
                'domain'     => $vhost->directiveContent('ServerName'),
                'aliases'    => $aliases,
                'admin'      => $vhost->directiveContent('ServerAdmin'),
                'dir'        => $vhost->directiveContent('DocumentRoot'),
                'error_log'  => $vhost->directiveContent('ErrorLog'),
                'access_log' => $vhost->directiveContent('CustomLog'),
                'file'       => $this->file,
            ];

        }
        return $websites;
    }
}
