<?php

namespace App\File;

/**
 * Nginx config
 */
class NginxConf
{
    /**
     * Construct
     *
     * @param string $file File.
     */
    function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Get Websites
     *
     * @return array
     */
    public function getWebsites()
    {
        $content = file_get_contents($this->file);
        $content = preg_replace('/\#[^\n]*\n/m', "\n", $content);


        if (! preg_match_all('/server\s*{/', $content, $matches, PREG_OFFSET_CAPTURE) ) {
            return [];
        }
        $websites = [];
        $matches[0] = array_reverse($matches[0]);
        $prev_pos = strlen($content);

        foreach ($matches[0] as $match) {
            $pos = $match[1];
            $part = substr($content, $pos, $prev_pos - $pos);
            $website = $this->getWebsite($part);
            if ($website) {
                $websites[] = $website;
            }
            $prev_pos = $pos;
        }
        $websites = array_reverse($websites);
        return $websites;
    }

    /**
     * Get Website
     *
     * @param string $part Content part.
     *
     * @return array        Website.
     */
    public function getWebsite($part)
    {

        $domains = [];
        if (preg_match('/server_name\s+([^;]+)/m', $part, $matches)) {
            $list = preg_replace('/[\s+]/', ':', $matches[1]);
            $domains = explode(':', $list);
        }
        $domains = array_filter($domains);
        $dir = '';
        if (preg_match('/root\s+([^;]+)/m', $part, $matches)) {
            $dir = $matches[1];
        }
        $listen = '';
        if (preg_match('/listen\s+([^;]+)/m', $part, $matches)) {
            $listen = $matches[1];
        }

        return [
            'domain'  => ! empty($domains) ? array_shift($domains) : [],
            'aliases' => $domains,
            'dir'     => $dir,
            'listen'  => $listen,
            'file'    => $this->file,
        ];
    }
}