<?php

namespace App\File;

use Illuminate\Support\Collection;

/**
 *
 */
class ConfigFile extends File
{
  protected $root;
  protected $server_name;

  public function get_root() {
    return $this->root;
  }
  public function set_root( $root ) {
    $this->root = $root;
  }

  public function get_server_name() {
    return $this->server_name;
  }
  public function set_server_name( $server_name ) {
    $this->server_name = $server_name;
  }
  /**
   * Get By Dir
   * @param  string  $dir
   * @param  boolean $conf_dir
   * @return Collection
   */
  static function get_by_dir( $dir, $conf_dir = false ) {
    // Validate dir
    if ( $dir && ! is_dir( $dir ) ) {
      return null;
    }
    // Establish the dir path
    if ( ! $dir ) {
      $dir = getcwd();
    }
    if ( is_dir( "$dir/public" ) ) {
      $dir = "$dir/public";
    }

    // Determine the config directory
    if ( ! $conf_dir ) {
      $conf_dir = env( 'HOST_CONF_DIR', 'DEFAULT' );
      if ( 'DEFAULT' === $conf_dir ) {
        if ( $use_nginx ) {
          $conf_dir = '/etc/nginx/sites-available';
        } else {
          $conf_dir = '/etc/apache2/sites-available';
        }
      }
    }

    $use_nginx = env( 'HOST_WEBSERVER', 'apache' ) == 'nginx';
    $files = self::search_in_dir( $dir, $conf_dir.'/*' );
    $domain = false;
    $configs = [];
    if ( empty( $files ) ) {
      return null;
    }

    $collection = new Collection();
    foreach ( $files as $file ) {
      $config = new ConfigFile( $file );
      if ( $use_nginx ) {
        $config->set_root( self::search_in_file( 'root', $file ) );
        $config->set_server_name( self::search_in_file( 'server_name', $file ) );
      } else {
        $config->set_root( self::search_in_file( 'DocumentRoot', $file ) );
        $config->set_server_name( self::search_in_file( 'ServerName', $file ) );
      }
      $collection->push( $config );
    }

    return $collection;
  }

  static function search_in_dir( $needle, $pattern ) {
    $needle .= "\n";
    $files = glob( $pattern );
    $matches = array();
    foreach ( $files as $file ) {
      $handle = @fopen( $file, "r");
      if ($handle) {
        while (!feof($handle)) {
          $buffer = fgets( $handle );
          if ( strpos( $buffer, $needle ) !== FALSE ) {
            $matches[] = $file;
            break;
          }
        }
        fclose($handle);
      }
    }
    return $matches;
  }

  static function search_in_file( $var, $file ) {
    $handle = @fopen( $file, "r");
    $server_name = false;
    while (!feof($handle)) {
      $buffer = trim( fgets( $handle ) );
      if ( ! preg_match('/^'. $var .'\s+(.*)$/', $buffer, $matches ) ) {
        continue;
      }
      $server_name = $matches[1];
      break;
    }
    fclose($handle);
    return $server_name;
  }
}