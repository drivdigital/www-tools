<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Server extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ningx_status'          => $this->ningx_status,
            'uptime'                => $this->uptime,
            'distro'                => $this->distro,
            'disk_space_available'  => $this->disk_space_available,
            'disk_capacity'         => $this->disk_capacity,
            'pear_config_installed' => $this->pear_config_installed,
            'versions'              => $this->versions,
            'assertions'            => $this->assertions,
            'websites'              => $this->websites,
        ];
    }
}
