<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\AppInit::class,
        \App\Console\Commands\AppAddInstance::class,
        \App\Console\Commands\AppAddDatabase::class,
        \App\Console\Commands\MysqlDatabase::class,
        \App\Console\Commands\MysqlDatabases::class,
        \App\Console\Commands\MysqlDeleteDatabase::class,
        \App\Console\Commands\MysqlEmptyDatabase::class,
        \App\Console\Commands\MysqlImport::class,
        \App\Console\Commands\MysqlUsers::class,
        \App\Console\Commands\MysqlUserGrant::class,
        \App\Console\Commands\MysqlUserCreate::class,
        \App\Console\Commands\Host\HostSetupFolder::class,
        \App\Console\Commands\Host\HostConfApply::class,
        \App\Console\Commands\Host\HostConfCreate::class,
        \App\Console\Commands\Host\HostWizard::class,
        \App\Console\Commands\Host\HostMove::class,
        \App\Console\Commands\MysqlBackup::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
