<?php

namespace App\Console\Commands;

use App\Mysql\User;
use Illuminate\Console\Command as BaseCommand;

class Command extends BaseCommand
{
  public function fill_arguments( $fields ) {
    $arguments = [];
    foreach ($fields as $key => $suggestion ) {
      $arguments[$key] = $this->fill_argument( [
        'key'        => $key,
        'suggestion' => $suggestion,
        'method'     => 'ask',
      ] );
    }
    return $arguments;
  }
  public function fill_argument( $field ) {
    if ( ! isset( $field['method'] ) ) {
      $field['method'] = 'ask';
    }
    if ( ! isset( $field['suggestion'] ) ) {
      $field['suggestion'] = null;
    }
    if ( ! isset( $field['label'] ) ) {
      $field['label'] = ucfirst( $field['key'] ) .'?';
    }
    $var = $this->option( $field['key'] );
    if ( ! $var ) {
      if ( 'ask' == $field['method'] ) {
        $var = $this->ask( $field['label'], $field['suggestion'] );
      } elseif ( 'confirm' == $field['method' ] ) {
        $var = $this->confirm( $field['label'], $field['suggestion'] );
      } else {
        throw new \Exception("Invalid method, {$field['method']}");
      }
    }
    return $var;
  }
}
