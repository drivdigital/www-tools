<?php

namespace App\Console\Commands;

use App\Mysql\Database;
use Illuminate\Console\Command;

class MysqlDeleteDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:delete-database {slug} {branch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all tables in a database';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $slug = $this->argument('slug');
        $branch = $this->argument('branch');

        $db = new Database( $slug, $branch );
        if ( ! $db->exists() ) {
            $this->info( "Sorry lad, there is no database for $slug, $branch" );
        }
        $db->delete();
        $this->info( "All done" );
    }
}
