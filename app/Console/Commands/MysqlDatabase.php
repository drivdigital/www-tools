<?php

namespace App\Console\Commands;

use App\Mysql\Database;
use Illuminate\Console\Command;

class MysqlDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:database {slug} {branch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a database with a database user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $slug = $this->argument('slug');
        $branch = $this->argument('branch');

        $db = new Database( $slug, $branch );
        $db->save();
        $user = $db->get_user();
        if ( $user->exists() ) {
            $this->error( 'Deleting the user' );
            $user->delete();
        }
        if ( $user->exists() ) {
            $this->error( 'The user exists already' );
            return;
        }
        if ( ! $user->save() ) {
            $this->error( 'Could not save the user' );
            return;
        }
        $user->grant();
        $format = "Database:%s\nUsername:%s\nPassword:%s\n";
        $this->info( sprintf(
            $format,
            $db->get_combined_name(),
            $user->username,
            $user->password
        ) );
    }
}
