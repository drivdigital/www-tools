<?php

namespace App\Console\Commands;

use App\Mysql\User;

class MysqlUserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:user-create {--username=} {--password=} {--host=localhost} {--superadmin=} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->fill_arguments( [
          'username' => false,
          'host'     => 'localhost',
        ] );
        extract( $arguments );
        $user = new User( $username, $host );
        if ( $user->exists() ) {
            if ( $this->fill_argument( [
              'key'        => 'force',
              'label'      => 'Do you want to delete and re-create this user?',
              'method'     => 'confirm',
              'suggestion' => false,
            ] ) ) {
              $user->delete();
            } else {
              $this->error( 'The user already exist!' );
              return;
            }
        }

        $password = $this->fill_argument( [
          'key'        => 'password',
          'suggestion' => false,
        ] );
        if ( ! $user->save() ) {
          $this->error( "Mysql could not save the user, $username!" );
        }
        $this->comment( "The user $username has been saved" );
        $superadmin = $this->fill_argument( [
          'key'        => 'superadmin',
          'method'     => 'confirm',
        ] );
        if ( $superadmin ) {
          $user->grant( '*', 'ALL PRIVILEGES', true );
          $this->info ( "All privileges granted" );
        }
        if ( ! $password ) {
          $this->info( "Password: $user->password" );
        }
    }
}
