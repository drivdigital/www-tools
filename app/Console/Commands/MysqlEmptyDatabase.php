<?php

namespace App\Console\Commands;

use App\Mysql\Database;
use Illuminate\Console\Command;

class MysqlEmptyDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:empty-database {slug} {branch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all tables in a database';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $slug = $this->argument('slug');
        $branch = $this->argument('branch');

        $db = new Database( $slug, $branch );
        if ( ! $db->exists() ) {
            $this->info( "Sorry lad, there is no database for $slug, $branch" );
        }
        $db->disable_foreign_keys();
        foreach( $db->get_tables() as $table ) {
            $table = (array) $table;
            $table = reset( $table );
            $db->drop_table( $table );
            $this->info( "Dropped $table" );
        }
        $db->enable_foreign_keys();
        $this->info( "All done" );
    }
}
