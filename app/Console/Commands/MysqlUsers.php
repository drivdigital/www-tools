<?php

namespace App\Console\Commands;

use App\Mysql\User;
use App\Mysql\Database;
use Illuminate\Console\Command;

class MysqlUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List users';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::list();
        $databases = [];
        $db_records = Database::list();
        foreach ( $db_records as $record ) {
          $databases[] = $record->name;
        }
        foreach ( $users as $record ) {
          if ( in_array( $record->User, $databases ) ) {
            $this->comment( "{$record->User}@{$record->Host}" );
          } else {
            $this->info( "{$record->User}@{$record->Host}" );
          }
        }
    }
}
