<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class AppAddDatabase extends App
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:add-database {instance}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialise a new app';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = $this->get_app_json();
        $name = $this->argument( 'instance' );
        if ( ! isset( $json['instances'][ $name ] ) ) {
            $this->error( 'No such instance, '.$name );
        }
        if ( ! isset( $json['instances'][ $name ]['databases'] ) ) {
            $json['instances'][ $name ]['databases'] = [];
        }
        $database_name = $this->info( 'Give the database a project id. This is useful because some hosting providers use generated names for databases.' );
        $database_name = $this->ask( 'Database id, (eg. wordpress)' );
        $database = [
            'type' => $this->anticipate( 'Database type', ['mysql'] ),
            'exclude_data' => [],
            'credentials' => [
                'database' => [],
                'username' => [],
                'password' => [],
            ],
        ];
        while ( $this->confirm( 'Exclude data from a table?' ) ) {
            $database['exclude_data'][] = $this->ask( 'Table name' );
        }
        foreach ( $database['credentials'] as $key => &$value ) {
            if ( $key !== 'password' ) {
                $value = $this->ask( ucfirst( $key ) );
            }
            $variable = $this->ask( "Custom variable for $key?", false );
            if ( $variable ) {
                $value = [
                    'value' => $value,
                    'variable' => $variable,
                ];
            }
        }

        $json['instances'][ $name ]['databases'][ $database_name ] = $database;
        $this->save_app_json( $json );
    }
}
