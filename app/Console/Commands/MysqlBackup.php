<?php

namespace App\Console\Commands;

use App\Mysql\Database;
use App\Mysql\Mysql;
use Illuminate\Console\Command;

class MysqlBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:backup {slug?} {branch?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create backups for all sites';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $slug = $this->argument('slug');
        $branch = $this->argument('branch');
        $mysql = new Mysql();
        $ignore = [
            'information_schema',
            'performance_schema',
            'mysql',
        ];
        $databases = $mysql->exec( ['SHOW DATABASES'] );
        $year = date( 'Y' );
        $month = date( 'm' );
        $day = date( 'd' );
        $location = env( 'MYSQL_BACKUP_DIR' );
        if ( ! $location ) {
            $this->error( 'You have to define MYSQL_BACKUP_DIR in the .env file first' );
            return;
        }

        if ( 'local' != env( 'APP_ENV' ) ) {
            $this->set_dir( "$location" );
            $this->set_dir( "$location/$year" );
            $this->set_dir( "$location/$year/$month" );
            $this->set_dir( "$location/$year/$month/$day" );
        }

        foreach ( $databases as $record ) {
            $database = $record->Database;
            if ( in_array( $database, $ignore ) ) {
                continue;
            }
            $file = "$location/$year/$month/$day/$database.sql.gz";
            if ( ! file_exists( $file ) ) {
                $this->info( "Backing up $file" );
                if ( 'local' == env( 'APP_ENV' ) ) {
                    $this->info( "mysqldump $database | gzip > $file" );
                } else {
                    `mysqldump $database | gzip > $file`;
                }
            }
        }
    }


    public function set_dir( $path ) {
      if ( !file_exists( $path ) ) {
        mkdir( $path );
      }
      if ( !file_exists( $path ) ) {
        die( 'Could not create the downloads dir' );
      }
      return $path;
    }
}
