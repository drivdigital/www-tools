<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

abstract class App extends Command
{
    public function get_app_json() {
        $cwd = getcwd();
        $json = [];
        if ( file_exists( "$cwd/app.json" ) ) {
            $json = json_decode( file_get_contents( "$cwd/app.json" ), 1 );
        }
        if ( ! $json ) {
            $json = [];
        }
        return $json;
    }
    public function save_app_json( $json, $file = false ) {
        $cwd = getcwd();
        if ( ! $file ) {
            $file = "$cwd/app.json";
        }
        file_put_contents( $file, json_encode( $json, JSON_PRETTY_PRINT ) );
    }
}
