<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class AppInit extends App
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialise a new app';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = $this->get_app_json();
        $json['name'] = $this->ask( 'Project name' );
        $type = $this->anticipate( 'Type (wordpress, magento, laravel, etc..)', ['wordpress','magento','laravel'] );
        $json['type'] = strtolower( $type );
        $this->save_app_json( $json );
        while ( $this->confirm( 'Do you want to add a new instance?', false ) ) {
            $this->call( 'app:add-instance' );
        }

    }
}
