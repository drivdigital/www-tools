<?php

namespace App\Console\Commands\Host;
use Illuminate\Console\Command;

class HostMove extends Host
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'host:move
        {arg1?}
        {arg2?}
        {--f|from=}
        {--t|to=}
        {--C|company-from=}
        {--c|company-to=}
        {--B|branch-from=}
        {--b|branch-to=}
        {--D|domain-from=}
        {--d|domain-to=}
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move a host';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $this->args = [];
      if ( $this->argument( 'arg1' ) ) {
        $this->args[] = $this->argument( 'arg1' );
      }
      if ( $this->argument( 'arg2' ) ) {
        $this->args[] = $this->argument( 'arg2' );
      }


      $dir_from = $this->get_from_dir();
      $dir_to = $this->get_to_dir();
      // Step 1
      // Move the folder from A to B
      $this->move_folder( $dir_from, $dir_to );

      // Step 2
      // Rename the nginx/apache config
      $this->move_config( $dir_from );

      // Step 3
      // Rename the database
    }

    /**
     * Move Folder
     */
    public function move_folder( $dir_from, $dir_to ) {
      $parameters = $this->get_parameters();
      $this->info   ( 'The original directory,' );
      $this->comment( ' '. $dir_from );
      $this->info   ( 'Will be moved to,' );
      $this->comment( ' '. $dir_to );
      if ( ! $this->confirm( 'Do you want to move the directory?', true ) ) {
        return false;
      }
      // exec( 'mv '. escapeshellarg( $dir_from ) .' '. escapeshellarg( $dir_to ) );
      return true;
    }

    /**
     * Get From Dir
     * @return string
     */
    protected function get_from_dir() {
      $dir = $this->option('from');
      if ( $dir && is_dir( $dir ) ) {
        return $dir;
      }
      if ( ! $dir && count( $this->args ) == 2 ) {
        $dir = $this->args[0];
        if ( ! is_dir( $dir ) ) {
          $this->error( 'The provided path does not exist!' );
          $dir = false;
        }
      }
      if ( ! $dir ) {
        // @TODO: Build path from company and branch
        // (With prediction!)
        // $company_from = $this->option('company-from');
        // $branch_from  = $this->option('branch-from');
      }
      if ( ! $dir ) {
        $dir = $this->ask( 'Which project would you like to move?', getcwd() );
      }
      // Check if it's a valid project
      $project = \App\File\Project::get_from_path( $dir );
      if ( ! $project ) {
        $this->error( 'The provided path is not a valid web project!' );
        $this->error( $dir );
        return $dir;
      }
      return $project->get_path();
    }

    /**
     * Get To Dir
     * @return string
     */
    protected function get_to_dir() {
      $dir = $this->option('to');
      if ( $dir && is_dir( $dir ) ) {
        return $dir;
      }
      if ( ! $dir && count( $this->args ) == 1 ) {
        $dir = $this->args[0];
      }
      if ( ! $dir && count( $this->args ) == 2 ) {
        $dir = $this->args[1];
      }
      if ( file_exists( $dir ) ) {
        $this->error( 'The destination already exists!' );
        $this->error( $dir );
        $dir = false;
      }
      if ( ! $dir ) {
        $dir = $this->ask( 'Where do you want to move the folder to?' );
      }
      if ( '/' != substr( $dir, 0, 1 ) ) {
        $dir = getcwd() .'/'. $dir;
      }
      return $dir;
    }

    public function move_config( $dir ) {
      $parameters  = $this->get_parameters();
      $config_file = \App\File\ConfigFile::get_by_dir( $dir );

      if ( ! $config_file ) {
        $this->warn( 'I could not determine any config file(s) for this directory!');
        $this->warn( 'Search was based on dir, "'. $dir .'".');
        $this->info( 'You will have to move the config files manually.' );
        return;
      }

      $domain_from = $this->get_from_domain( $config_file );
      $domain_to   = $this->get_to_domain();

    }

    /**
     * Get From Dir
     * @return string
     */
    protected function get_from_domain( $config_file ) {
      $from = $this->option('from');
      $company_from = $this->option('company-from');
      $company_from = $this->option('branch-from');
      if ( $from && is_dir( $from ) ) {
        return $from;
      }
    }

    /**
     * Get To Domain
     * @return string
     */
    protected function get_to_domain() {
      $company_to = $this->option('company-to');
    }
}
