<?php

namespace App\Console\Commands\Host;
use Illuminate\Console\Command;

abstract class Host extends Command
{
    public function get_parameters() {
        $args =  [
            // Unix information
            'user'                => env('HOST_USER', 'www-data'),
            'group'               => env('HOST_GROUP', 'www-data'),

            // Apache
            'conf_dir'            => env('HOST_CONF_DIR', 'DEFAULT'),
            'command_conf_enable' => env('HOST_COMMAND_CONF_ENABLE', 'a2ensite'),
            'command_conf_test'   => env('HOST_COMMAND_CONF_TEST', 'DEFAULT'),
            'command_restart'     => env('HOST_COMMAND_RELOAD', 'DEFAULT'),
        ];
        if (env('HOST_WEBSERVER', 'apache') == 'nginx') {
            $args = $this->set_default_nginx($args);
        } else {
            $args = $this->set_default_apache($args);
        }
        if (! preg_match('/\{company/', $this->signature)) {
            return $args;
        }
        $company = $this->argument('company');
        $slug = $this->argument('slug');
        $branch = $this->argument('branch');
        $args = array_merge($args, [
            // Base information
            'company'     => $company,
            'slug'        => $slug,
            'branch'      => $branch,
            // File & directory paths
            'company_dir' => "/var/www/$company",
            'project_dir' => "/var/www/$company/$slug",
            'tree_dir'    => "/var/www/$company/$slug/$branch",
            'full_dir'    => "/var/www/$company/$slug/$branch/public",
        ]);
        return $args;
    }

    public function set_default_nginx($args) {
        if ('DEFAULT' === $args['conf_dir']) {
            $args['conf_dir'] = '/etc/nginx/sites-available';
        }
        if ('DEFAULT' === $args['command_restart']) {
            $args['command_restart'] = 'service nginx restart';
        }
        if ('DEFAULT' === $args['command_conf_test']) {
            $args['command_conf_test'] = 'nginx -t';
        }
        return $args;
    }
    public function set_default_apache($args) {
        if ('DEFAULT' === $args['conf_dir']) {
            $args['conf_dir'] = '/etc/apache2/sites-available';
        }
        if ('DEFAULT' === $args['command_restart']) {
            $args['command_restart'] = 'apachectl restart';
        }
        if ('DEFAULT' === $args['command_conf_test']) {
            $args['command_conf_test'] = 'apachectl configtest';
        }
        return $args;
    }
}
