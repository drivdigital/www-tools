<?php

namespace App\Console\Commands\Host;
use App\Mysql\Database;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Question\Question;

use phpseclib\Net\SSH2;
use phpseclib\Crypt\RSA;

class HostWizard extends Host
{
    protected $domain  = false;
    protected $company = false;
    protected $slug    = false;
    protected $branch  = false;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'host:wizard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create folder structure for a new host';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path    = false;
        $user = trim( `whoami` );
        $continue = true;
        if ( 'root' !== $user ) {
          $continue = $this->confirm( "This command should be run as root. Some parts of the script will probably not work. Continue anyway?", false );
        }
        if ( ! $continue ) {
          return;
        }

        $parameters  = $this->get_parameters();


        // Determine config from apache config
        // Search for CWD in the config files and match configuration
        $config = $this->determine_config();
        if ( $config ) {
          $this->comment( "Detected config in {$config['file']}" );
          $this->info( "Domain:\t{$config['domain']}" );
          $this->info( "Path:\t{$config['path']}" );
          $this->info( "CWD:\t". getcwd() );
          if ( $this->confirm( "Is this {$config['domain']}", true ) ) {
            $this->domain = $config['domain'];
            $this->path   = $config['path'];
          }

          // @TODO: What task do you want to do?
          // 1: Everything
          // 2: Apache Fix
          // 3: Database Fix
          // 4: Clear the database and import a new one
        }

        $suggestion  = null;
        $maybe_url = getcwd();
        $maybe_url = preg_replace('/\/public\/?$/', '', $maybe_url );
        $maybe_url = basename($maybe_url);
        if ( strpos( $maybe_url, '.' ) !== false ) {
          $suggestion = $maybe_url;
        }
        // Either domain was not detected or declined
        if ( ! $this->domain ) {
          $this->domain = $this->domain( 'What domain do you want to use?', $suggestion );
        }

        $suggestion = null;

        if ( preg_match( '/^([a-z\d\-]+)\.[a-z]+$/', $this->domain, $matches ) ) {
            $suggestion = $matches[1];
        }

        $this->info( 'This script prefers to use this folder structure [/var/www/COMPANY/SLUG/BRANCH/public] for projects.' );
        $prefered_branch = in_array( env( 'APP_ENV' ), [ 'local', 'stage', 'staging', 'develop', 'development' ] ) ? 'develop' : 'master';
        $prefered_branch = env( 'PREFERED_BRANCH' ) ?: $prefered_branch;

        $this->company     = $this->ask( "What company name should this be under?", $suggestion );
        $this->slug        = $this->ask( "What slug would you like to call this project?", $suggestion );
        $this->branch      = $this->ask( "What branch is this project using?", $prefered_branch );

        if ( ! $path ) {
          // No path yet
          $new_folder  = $this->confirm( "Do you want to generate a new folder for this project?", $prefered_branch == 'master' );
          if ( $new_folder ) {
            // Make a new path
            $this->info( 'Creating folder structure.' );
            $this->call( 'host:folder', [
              'company' => $this->company,
              'slug'    => $this->slug,
              'branch'  => $this->branch
            ] );
          }
        }

        // Go ahead and configure apache
        $this->create_apache_config( $path, $new_folder );

        // Does the database exist already?
        $mysql_database = new Database( $this->slug, $this->branch );
        $database = false;
        if ( ! $mysql_database->exists() ) {
          $database = $this->confirm( "Do you want a new database for this website?", true );
        } else {
          $this->comment( 'Database, '. $mysql_database->get_combined_name() .', already exists.' );
          $database = $this->confirm( "Overwrite the database user?", false );
        }
        if ( $database ) {
          $this->info( "Database credentials:" );
          $this->call( 'mysql:database', [
            'slug'    => $this->slug,
            'branch'  => $this->branch,
          ] );
        }

        $activate = $this->confirm( "Restart apache and activate the site?", true );

        if ( $activate ) {
            $this->info( "Applying apache config" );
            $this->call( 'host:conf-apply' );
        }

        if ( 'local' == env( 'APP_ENV' ) ) {
          $this->legacy_checks();
        }
        $import = $this->ask( 'Do you want to import a database. Either a .sql or .sql.gz file.', false );
        if ( $import) {
          $this->call( 'mysql:import', [
            'slug'    => $this->slug,
            'branch'  => $this->branch,
            'file'    => $import
          ] );
        }

        if ( 'local' == env( 'APP_ENV' ) && preg_match( '/\.x$/', $this->domain ) ) {
            $in_hosts = trim( `grep {$this->domain} /etc/hosts` );
            if ( ! $in_hosts ) {
                $this->info( "Adding {$this->domain} to your hosts file." );
                file_put_contents( '/etc/hosts', "\n127.0.0.1\t{$this->domain}", FILE_APPEND );
            }
        }

        $this->info( "Your project is up and running on {$this->domain}. Please check DNS settings or hosts file if it's not working" );


    }

    public function domain( $question, $suggestion = null ) {
      return $this->ask( $question, $suggestion, __CLASS__.'::valid_domain' );
    }

    public function create_apache_config( $path = false, $new_folder = false ) {
      $parameters = $this->get_parameters();
      $file_name = "{$parameters['conf_dir']}/{$this->domain}.conf";
      if ( file_exists( $file_name ) ) {
        $this->comment( "The {$this->domain}.conf already exists @ $file_name." );
        $config = file_get_contents( $file_name );
        $this->info( $config );
        if ( ! $this->confirm( 'Do you want to overwrite the existing config?' ) ) {
          return;
        }
      }
      if ( $new_folder ) {
        // We just made a new folder. Use that
        $this->info( "Creating an apache config file for {$this->domain}." );
        $this->call( 'host:conf-create', [
          'company' => $this->company,
          'slug'    => $this->slug,
          'branch'  => $this->branch,
          'url'     => $this->domain,
        ] );
      } else {

        // Ask for a path if there is none
        if ( ! $path ) {
          $suggestion = getcwd();
          if ( is_dir( "$suggestion/public" ) ) {
              $suggestion = "$suggestion/public";
          }
          $path = $this->ask( "Please provide a directory path for {$this->domain}.", $suggestion );
        }
        // Create a path if it's provided
        if ( $path ) {
          $this->info( "Creating an apache config file for {$this->domain}." );
          $this->call( 'host:conf-create', [
            'company' => $this->company,
            'slug'    => $this->slug,
            'branch'  => $this->branch,
            'url'     => $this->domain,
            'path'    => $path,
          ] );
        } else {
          $this->info( 'No path was given, so an apache config file is not created.' );
        }
      }
    }

    static public function valid_domain( $domain ) {
        $domain = preg_replace( '/^https?:\/\//', '', $domain );
        // throw new InvalidArgumentException( 'You need to provide a valid domain name' );
        return $domain;
    }

    public function determine_config() {
      $cwd = getcwd();
      if ( is_dir( "$cwd/public" ) ) {
        $cwd = "$cwd/public";
      }
      $parameters = $this->get_parameters();
      $files = $this->search_in_dir( $cwd, $parameters['conf_dir'].'/*' );
      $domain = false;
      $configs = [];
      if ( ! empty( $files ) ) {
        foreach ( $files as $file ) {
          $configs[] = [
            'file'   => $file,
            'path'   => $this->get_conf_var( 'DocumentRoot', $file ),
            'domain' => $this->get_conf_var( 'ServerName', $file ),
          ];
        }
      }
      if ( empty( $configs ) ) {
        return null;
      }
      if ( count( $configs ) > 1 ) {
        $this->error( 'Too many configs for this folder' );
        foreach ( $configs as $config ) {
          $this->error( "\t• file:\t{$config['file']}" );
          $this->error( "\t  domain:\t{$config['domain']}" );
        }
        die;
      }
      return reset( $configs );
    }

    public function get_conf_var( $var, $file ) {
      $handle = @fopen( $file, "r");
      $server_name = false;
      while (!feof($handle)) {
        $buffer = trim( fgets( $handle ) );
        if ( ! preg_match('/^'. $var .'\s+(.*)$/', $buffer, $matches ) ) {
          continue;
        }
        $server_name = $matches[1];
        break;
      }
      fclose($handle);
      return $server_name;
    }

    public function search_in_dir( $needle, $pattern ) {
      $needle .= "\n";
      $files = glob( $pattern );
      $matches = array();
      foreach ( $files as $file ) {
        $handle = @fopen( $file, "r");
        if ($handle) {
          while (!feof($handle)) {
            $buffer = fgets( $handle );
            if ( strpos( $buffer, $needle ) !== FALSE ) {
              $matches[] = $file;
              break;
            }
          }
          fclose($handle);
        }
      }
      return $matches;
    }

    /**
     * Legacy checks
     */
    public function legacy_checks() {
      // @TODO: Refactor as a command
      $files = [
        'vagrant-config/config.json',
        '../vagrant-config/config.json',
      ];
      foreach ( $files as $file ) {
        if ( ! file_exists( $file ) ) {
          continue;
        }
        $json = file_get_contents( $file );
        $data = json_decode(  $json, true );
        if ( ! isset( $data['ssh_live'] ) ) {
          continue;
        }
        $this->comment( 'There is a vagrant file with SSH information' );
        if ( ! $this->confirm( 'Try to import the database from the live server?', false ) ) {
          return;
        }
        $this->import_database_from_server( $data['ssh_live'] );
      }
    }

    /**
     * ENV
     * @param  string $key
     * @param  string $question
     * @return string
     */
    public function env( $key, $question, $default = null ) {
      $value = env( $key, null );
      if ( null !== $value ) {
        return $value;
      }
      $value = $this->ask( $question, $default );
      return $value;
    }
    /**
     * Import database from server
     * @param  string $login_host
     */
    public function import_database_from_server( $login_host ) {
      $rsa_file = $this->env( 'PRIVATEKEY_FILE', 'Please give me the path to your private file' );
      // @TODO: Refactor as a command
      if ( ! file_exists( $rsa_file ) ) {
        $this->error( 'No ssh-key found. Skipping database download' );
        return;
      }
      $parts = explode( '@', $login_host );
      if ( count( $parts ) != 2 ) {
        $this->error( "The ssh addres, $login_host, should contain username and host separated by @. Skipping database download" );
        return;
      }
      $key = new RSA();
      $key->setPassword( $this->env( 'PRIVATEKEY_PASSPHRASE', 'What is the privatekey passphrase') );
      // $key->setPublicKeyFormat(RSA::PUBLIC_FORMAT_OPENSSH);
      if ( ! $key->loadKey( file_get_contents( $rsa_file ) ) ) {
        $this->error('WRONG');
        die;
      };
      $ssh = new SSH2( $parts[1] );
      if ( ! $ssh->login( $parts[0], $key ) ) {
        var_dump( file_get_contents( '/Users/forsvunnet/.ssh/id_rsa' ) );
        var_dump( $parts );
        var_dump( $ssh->getServerIdentification() );
        var_dump( $ssh->getErrors() );
        var_dump( $ssh->getStdError() );
        exit('Login Failed');
      }

      $this->comment( 'Teach me how to sniff credentials first!' );
      return;

      // Create a new file with this:
      // 'mysqldump -u %username %database -p%password | gzip';

      // Chmod it to 404
      $this->comment( 'Downloading the live database' );
      $content = $ssh->exec( '.dbdump' );
      file_put_contents( "/tmp/{$this->domain}.sql.gz", $content );
      $this->call( 'mysql:empty-database',[
        'slug'    => $this->slug,
        'branch'  => $this->branch,
      ] );
      $this->call( 'mysql:import',[
        'slug'    => $this->slug,
        'branch'  => $this->branch,
        'file'    => "/tmp/{$this->domain}.sql.gz",
      ] );

      $ssh->disconnect();
    }

}
