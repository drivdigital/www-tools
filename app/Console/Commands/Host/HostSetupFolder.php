<?php

namespace App\Console\Commands\Host;
use Illuminate\Console\Command;

class HostSetupFolder extends Host
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'host:folder {company} {slug} {branch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create folder structure for a new host';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parameters = $this->get_parameters();

        $commands = [
            "mkdir -p {$parameters['full_dir']}",
            "chown {$parameters['user']}:{$parameters['group']} {$parameters['company_dir']}",
            "chown {$parameters['user']}:{$parameters['group']} {$parameters['project_dir']}",
            "chown -R {$parameters['user']}:{$parameters['group']} {$parameters['tree_dir']}",
        ];
        foreach ( $commands as $command ) {
            if ( 'local' === env( 'APP_ENV' ) ) {
                echo "$command\n";
                echo shell_exec( $command );
            } else {
                echo shell_exec( $command );
            }
        }
    }
}
