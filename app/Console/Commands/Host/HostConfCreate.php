<?php

namespace App\Console\Commands\Host;
use Illuminate\Console\Command;

class HostConfCreate extends Host
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'create';
    protected $signature = 'host:conf-create
        {company}
        {slug}
        {branch}
        {url}
        {path?}
        {--f|force : Overwrite exisiting config file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create vhost config';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $force = false;
        $parameters = $this->get_parameters();
        $url = $this->argument('url');
        $force = $this->option('force');
        $path = $this->argument('path');

        if ( $path ) {
            $parameters['full_dir'] = $path;
        }

        if ( preg_match( '/^https:\/\//', $url ) ) {
            $this->error( 'URL\'s using https/SSL is currently not supported.');
            return false;
        }
        if ( ! preg_match( '/^http:\/\//', $url ) ) {
            $url = 'http://'. $url;
        }

        $parts = parse_url( $url );
        if ( ! isset( $parts['host'] ) || ! isset( $parts['scheme'] ) ) {
            $this->error( 'Invalid url! '.$url );
            return;
        }
        $domain = $parts['host'];
        $url = $parts['scheme'].'://'. $parts['host'];
        $ext = '.conf';
        if ( env( 'HOST_WEBSERVER', 'apache' ) == 'nginx' ) {
            $ext = '';
        }
        $file_name = "{$parameters['conf_dir']}/{$domain}{$ext}";

        if ( file_exists( $file_name ) ) {
            if (  ! $force && ! $this->confirm( 'There is a config file already. Do you want to overwrite it?', true ) ) {
                $this->error( 'File exists, stopping' );
                return;
            }
            $this->error( 'Warning: File exists, overwriting' );
        }

        $this->info( "Creating a new config for $url at $file_name");

        $conf = $this->get_conf( $domain, $parameters['full_dir'] );
        file_put_contents( $file_name, $conf );
        $created = file_exists( $file_name );

        if ( ! $created ) {
            $this->error( "Could not create $file_name" );
            return;
        }
        if ( env( 'HOST_WEBSERVER', 'apache' ) == 'nginx' ) {
            `ln -s $file_name /etc/nginx/sites-enabled/`;
            return;
        }
        if ( ! $parameters['command_conf_enable'] ) {
            return;
        }
        $result = exec( $parameters['command_conf_enable'] .' '. $domain, $output, $return_var );
        if ( $return_var ) {
            $this->error( print_r( $output, 1 ) );
            return;
        }
    }


    public function get_conf( $domain, $dir, $aliases = [], $admin = 'post@drivdigital.no' ) {
        if ( env( 'HOST_WEBSERVER', 'apache' ) == 'nginx' ) {
            $aliases = trim( implode( " ", $aliases) );
            $aliases = $aliases ? ' ' . $aliases : '';

            return trim( "
server {
    listen 80;
    server_name {$domain}{$aliases};
    root $dir;
    index index.php;
    include snippets/website.conf;
}

            " )."\n";
        }
        foreach ($aliases as &$alias) {
            $alias = "\tServerAlias $alias";
        }
        $aliases = implode( "\n", $aliases);
        return trim( "


<VirtualHost *:80>
    ServerName $domain
    $aliases
    ServerAdmin $admin
    DocumentRoot $dir
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
</VirtualHost>


        " )."\n";
    }
}
