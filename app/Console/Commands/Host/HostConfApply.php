<?php

namespace App\Console\Commands\Host;
use Illuminate\Console\Command;

class HostConfApply extends Host
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'create';
    protected $signature = 'host:conf-apply';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Apply vhost config';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parameters = $this->get_parameters();

        $result = exec( $parameters['command_conf_test'], $output, $return_var );
        if ( $return_var ) {
            $this->error( $output );
            return;
        }
        $result = exec( $parameters['command_restart'], $output, $return_var );
        if ( $return_var ) {
            $this->error( $output );
        }
    }
}
