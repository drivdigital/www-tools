<?php

namespace App\Console\Commands;

use App\Mysql\Database;
use Illuminate\Console\Command;

class MysqlImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:import {slug} {branch} {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import an sql file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $slug = $this->argument('slug');
        $branch = $this->argument('branch');
        $file = $this->argument('file');

        $db = new Database( $slug, $branch );
        if ( ! $db->exists() ) {
            $this->error( 'The database does not exist!' );
            return;
        }
        if ( ! file_exists( $file ) ) {
            $this->error( 'The file does not exist!' );
            return;
        }

        $db->import( $file );
    }
}
