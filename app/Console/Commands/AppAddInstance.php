<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class AppAddInstance extends App
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:add-instance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialise a new app';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = $this->get_app_json();

        $name        = $this->anticipate( 'Server name', ['live','test'] );
        if ( isset( $json['instances'][ $name ] ) ) {
            $this->confirm( "There is already an instance for $name. Do you want to continue?" );
        }
        // $this->json[];
        $json['instances'][ $name ] = [
            'environment' => $this->choice( 'Environment', ['staging', 'production', 'development'] ),
            'domain'      => $this->ask( "Domain" ),
        ];
        $this->save_app_json( $json );

        $aliases     = [];
        while ( $this->confirm( 'Add alias?' ) ) {
            $aliases[] = $this->ask( "Alias" );
        }
        if ( ! empty( $aliases ) )
            $json['instances'][ $name ]['aliases'] = $aliases;

        $json['instances'][ $name ]['server'] = [
            'username' => $this->ask( 'Server username (leave blank for individual access)', false ),
            'host'     => $this->ask( 'Host' ),
            'dir'      => $this->ask( 'Default directory', false ),
        ];
        $json['instances'][ $name ]['databases'] = [];
        $this->save_app_json( $json );

        while ( $this->confirm( 'Add a database?' ) ) {
            $this->call( 'app:add-database', [
                'instance' => $name
            ] );
        }

        $this->save_app_json( $json );
    }
}
