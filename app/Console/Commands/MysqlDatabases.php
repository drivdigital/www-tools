<?php

namespace App\Console\Commands;

use App\Mysql\Database;
use Illuminate\Console\Command;

class MysqlDatabases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:databases';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $databases = Database::list();
        $longest = 0;
        foreach ( $databases as $record ) {
          $length = strlen( $record->name );
          if ( $length > $longest ) {
            $longest = $length;
          }
        }
        uasort( $databases, __CLASS__.'::sort_records' );
        foreach ( $databases as $record ) {
          $length = strlen( $record->name );
          $pad_length = $longest - $length;
          $this->comment( sprintf(
            "%s\t%s",
            str_pad( $record->name, $longest ),
            "{$record->size} Mb"
          ) );
        }
    }
    static function sort_records( $a, $b ) {
      return $a->size < $b->size;
    }
}
