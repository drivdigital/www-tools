<?php

namespace App\Console\Commands;

use App\Mysql\User;
use Illuminate\Console\Command;

class MysqlUserGrant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:user-grant {username} {host=localhost} {grants?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grant grants to a user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        $host = $this->argument('host');
        $grants = $this->argument('grants');

        $user = new User( $username, $host );
        if ( ! $user->exists() ) {
            $this->error( 'The user does not exist!' );
            return;
        }

        if ( $grants ) {
            $user->grant( false, $grants );
        } else {
            $user->grant();
        }
    }
}
