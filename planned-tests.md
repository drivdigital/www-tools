# Server / Node information + Changelog /w timestamps

Ubuntu version
Architecture
PHP version
Apache version
Apache server name
Mariadb version
Letsencryp version
UWF status port 80/443
List of apache2/sites-enabled

## Linode already does these:
Avg. CPU usage
Avg. Memory usage

## For each user:
* Installed public keys
* Logins

## Various checks
Match enabled sites against the config-template and warn when not the same

# Site information
HTTPS redirection
Image compression




---------------------------------
Old stuff:


## Database
Create database
Create database user
Grant database rights

## Directories
Create directory
Delete directory
List directories
Match path against configs
List orphaned directories

## www-config
Create config
Read config
List configs

## Backup
Backup config and system files
Backup site media files
Backup site database
Backup www-config database
Upload backup
Download backup

## Git
Git pull project
Git status

## Apache2
Create host
Create host with alias
Enable host
Disable host
Letsencrypt
