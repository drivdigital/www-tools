<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/server', function() {
    $server = new App\Server();
    return new \App\Http\Resources\Server($server);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
